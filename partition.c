#include <stdio.h>

void swap(int *a, int *b){
    int temp = 0;
    temp = *a;
    *a = *b;
    *b = temp;
}

void partition(int n,int a[n]){
    int i=1, move[n], c=1, k=n-c;
    for(int j = 0; j<n; j++){
        move[j] = 0;
    }
    while(i<n && k>i){
        k = n-c;
        int x = 0;
        if(a[i]>a[0])
            x = 1;
        while(a[k]>a[0] && x){
            k--;
            c++;
        } // looks for the element to replace with from the end and narrows the search area for the next run.
        if(k>i && x){
            swap(&a[i],&a[k]);
            c++;
            ++move[i]; ++move[k]; // tester
        }
        i++;
    }
    for(i=0;i<n;i++)
        printf("%d ",a[i]);
    printf("\n");
    for(int j = 0; j<n; j++)
        printf("%d ",move[j]);
}

int main()
{
    int n=16;
    //printf("Enter the number of elements\n");
    //scanf("%d",&n);
    int numbers[16] = {6,4,4,4,7,7,7,4,4,4,7,7,7,4,4,4};
    //printf("Enter the elements\n");
    //for(int i=0;i<n;i++)
    //    scanf("%d",&numbers[i]);
    partition(n,numbers);
        
}

//only two levels of nestin